Bootstrap: docker
From: ubuntu

%files
    ## We copy the files belonging to the planner to a dedicated folder in the image.
	. /planner	

%post

    ## The "%post"-part of this script is called after the container has
    ## been created with the "%setup"-part above and runs "inside the
    ## container". Most importantly, it is used to install dependencies
    ## and build the planner. Add all commands that have to be executed
    ## once before the planner runs in this part of the script.

    ## Install all necessary dependencies.
    apt-get update
    apt-get -y install make g++ bison flex wget zlib1g-dev git

    ## go to directory and make the planner
    cd /planner
    make release 


%runscript
    ## The runscript is called whenever the container is used to solve
    ## an instance.

    DOMAINFILE=$1
    PROBLEMFILE=$2
    PLANFILE=$3
    TIMELIM_SECS=$4
    MEMLIM_MIB=$5
    RSEED=$6
   
    # No colored output (breaks verification of printed plan), 
    # verbosity: normal information, no verbose or debug printing. 
    OPTIONS="-co=0 -v=2 -s=$RSEED"

    stdbuf -o0 -e0 /planner/lilotane $DOMAINFILE $PROBLEMFILE $OPTIONS 2>&1 | tee $PLANFILE


## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
%labels
Name        Lilotane
Description Lifted Logic for Task Networks: TOHTN Planner using incremental SAT Solving
Authors     Dominik Schreiber <dominik.schreiber@kit.edu>
SupportsRecursion yes
SupportsPartialOrder no
